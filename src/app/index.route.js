export function routerConfig ($stateProvider, $urlRouterProvider) {
  'ngInject';
  $stateProvider
    .state('calculator', {
      url: '/calculator',
      templateUrl: 'app/calculator/calculator.html',
      controller: 'CalculatorController',
      controllerAs: 'calculator'
    });

  $urlRouterProvider.otherwise('/calculator');
}
