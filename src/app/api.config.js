export function apiConfig (RestangularProvider) {
  'ngInject';
  // Конфигурируем API
  RestangularProvider.setBaseUrl('http://test-credcalc.intranet/api/v1/');
}
