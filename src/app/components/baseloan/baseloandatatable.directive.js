export function BaseLoanDataTableDirective() {
  'ngInject';

  let directive = {
    restrict: 'E',
    templateUrl: 'app/components/baseloan/baseloaddatatable.html',
    //controller: BaseLoanTableController,
    scope: {
      loan: '='
    },
    controllerAs: 'baseloantable',
    //bindToController: true,
    link: scopeLink
  };

  return directive;

  function scopeLink(scope, el, attr, vm) {

    scope.showed = [];

    scope.limit = {
      limit: 12,
      page: 1,
      pageDefault: 1
    };

    function getLoanData() {
      let [ from, to ] = [scope.limit.limit * (scope.limit.page - 1), scope.limit.limit * scope.limit.page];
      scope.showed = scope.loan.loanData.slice(from, to);
    }

    let watcher = scope.$watch('loan.loanData', () => {
      scope.limit.page = scope.limit.pageDefault;
      getLoanData();
    });

    scope.getLoanData = getLoanData;
    scope.limitOptions = [12, 24, 48];
  }
}
