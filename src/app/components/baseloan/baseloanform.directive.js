export function BaseLoanFormDirective() {
  'ngInject';

  let directive = {
    restrict: 'E',
    templateUrl: 'app/components/baseloan/baseloanform.html',
    controller: BaseLoanFormController,
    scope: {
      obj: '='
    },
    controllerAs: 'baseloanform',
    bindToController: true,
    link: scopeLink
  };

  return directive;

  function scopeLink(scope, el, attr, vm) {

  	scope.loan = vm.obj;
  }
}

class BaseLoanFormController {

  constructor($window, $scope, $log, LoanService) {
    'ngInject';

    $scope.submitForm = function(isValid){
      if (isValid) {
        calculateLoan($scope.loan);
      }
    };

    $scope.getPdfDocument = (isValid) =>{
      if(!isValid) return;
      $window.open(LoanService.getLoanPdfDocumentLink($scope.loan));
    };

    function calculateLoan (loan) {
      LoanService.getLoanCalculation(loan)
        .then(data => {
          $log.info('OK');
          $scope.loan.loanData = data;
        })
        .catch(error => {
          $log.error(error, error.data);
        });
    };

  }
}

