class Loan { 
  constructor(amount = 100000, rate = 11.9, term = 12, issuanceDate = null, payDay = 1) {
    'ngInject';

    this.amount = amount;
    this.rate = rate;
    this.term = term;
    this.issuanceDate = issuanceDate || new Date();
    this.payDay = payDay;
    this.loanData = [];
  }

  attributeLabels() {
    return {
      amount: 'Сумма кредита',
      rate: 'Процентная ставка',
      term: 'Срок в месяцах',
      issuanceDate: 'Дата кредита',
      payDay: 'День оплаты'
    };
  }

  attributeLabel(name) {
    return this.attributeLabels()[name];
  }

}

export class LoanService {
  constructor(moment, $log, Restangular, $httpParamSerializer) {
    'ngInject';

    this.serialize = $httpParamSerializer;
    this.api = Restangular;
    this.storage = this.api.all('loan');
    this.moment = moment;    
  }

  getInstance() {
    return new Loan();
  }

  getLoanCalculation(loan) {
    let send_loan = angular.copy(loan);
    send_loan.issuanceDate = this.moment(loan.issuanceDate).format('YYYY-MM-DD');

    //return this.storage.post(send_loan);

    return new Promise((resolve, reject) => {
        
          resolve([
            {"date":"2017-02-01","monthlyPayments":8928,
            "paymentsOfDebt":8362.52,
            "paymentsOfInterest":565.48,
            "debtBalance":91637.48},
            {"date":"2017-03-01","monthlyPayments":8928,
            "paymentsOfDebt":8021.17,"paymentsOfInterest":906.83,
            "debtBalance":83616.31},
            {"date":"2017-04-03","monthlyPayments":8928,
            "paymentsOfDebt":7952.78,"paymentsOfInterest":975.22,
            "debtBalance":75663.53},
            {"date":"2017-05-02","monthlyPayments":8928,"paymentsOfDebt":8152.5,
            "paymentsOfInterest":775.5,"debtBalance":67511.03},
            {"date":"2017-06-01","monthlyPayments":8928,"paymentsOfDebt":8212.2,
            "paymentsOfInterest":715.8,"debtBalance":59298.83},
            {"date":"2017-07-03","monthlyPayments":8928,"paymentsOfDebt":8257.35,
            "paymentsOfInterest":670.65,"debtBalance":51041.48},
            {"date":"2017-08-01","monthlyPayments":8928,"paymentsOfDebt":8404.86,
            "paymentsOfInterest":523.14,"debtBalance":42636.62},
            {"date":"2017-09-01","monthlyPayments":8928,"paymentsOfDebt":8460.87,
            "paymentsOfInterest":467.13,"debtBalance":34175.75},
            {"date":"2017-10-02","monthlyPayments":8928,"paymentsOfDebt":8553.56,
            "paymentsOfInterest":374.44,"debtBalance":25622.19},
            {"date":"2017-11-01","monthlyPayments":8928,"paymentsOfDebt":8656.33,
            "paymentsOfInterest":271.67,"debtBalance":16965.86},
            {"date":"2017-12-01","monthlyPayments":8928,"paymentsOfDebt":8748.12,
            "paymentsOfInterest":179.88,"debtBalance":8217.74},
            {"date":"2018-01-16","monthlyPayments":8351.34,"paymentsOfDebt":8217.74,
            "paymentsOfInterest":133.6,"debtBalance":0}]);
    });
    return this.storage.post(send_loan);
  }

  getLoanPdfDocumentLink(loan) {
    let send_loan = angular.copy(loan);
    send_loan.issuanceDate = this.moment(loan.issuanceDate).format('YYYY-MM-DD');
    delete send_loan.loanData;

    return `${this.api.configuration.baseUrl}/loan/get-document/?${this.serialize(send_loan)}`;
  }

}


