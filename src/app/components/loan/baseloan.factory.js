export class BaseLoanFactory {
  /*@ngInject*/
  
  constructor(amount = 100000, rate = 11.9, term = 12, issuanceDate = null, payDay = 1) {
    'ngInject';

    this.amount = amount;
    this.rate = rate;
    this.term = term;
    this.issuanceDate = issuanceDate || new Date();
    this.payDay = payDay;
    this.loanData = [];
  }

  attributeLabels() {
    return {
      amount: 'Сумма кредита',
      rate: 'Процентная ставка',
      term: 'Срок в месяцах',
      issuanceDate: 'Дата кредита',
      payDay: 'День оплаты'
    };
  }

  attributeLabel(name) {
    return this.attributeLabels()[name];
  }

}