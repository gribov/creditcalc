export function DateFilter(moment) {
  'ngInject';

  return (value, format) => moment(value).format(format);
}
