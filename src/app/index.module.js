/* global malarkey:false, moment:false */

import { config } from './index.config';
import { apiConfig } from './api.config';
import { routerConfig } from './index.route';
import { runBlock } from './index.run';
import { CalculatorController } from './calculator/calculator.controller';
import { LoanService } from '../app/components/loan/loan.service';
import { NavbarDirective } from '../app/components/navbar/navbar.directive';
import { BaseLoanFormDirective } from '../app/components/baseloan/baseloanform.directive';
import { BaseLoanDataTableDirective } from '../app/components/baseloan/baseloandatatable.directive.js';
import { DateFilter } from '../app/components/filters/date.filter.js';

import '../i18n/angular-locale';

angular.module('calculator', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize','ngMessages', 'ngAria', 'restangular', 'ui.router', 'ngMaterial', 'md.data.table', 'toastr'])
  .constant('moment', moment)
  .config(config)
  .config(apiConfig)
  .config(routerConfig)
  .run(runBlock)
  .service('LoanService', LoanService)
  .controller('CalculatorController', CalculatorController, LoanService)
  .directive('baseLoanForm', BaseLoanFormDirective)
  .directive('baseLoanDataTable', BaseLoanDataTableDirective)
  .directive('acmeNavbar', NavbarDirective)
  .filter('loanDate', DateFilter);
