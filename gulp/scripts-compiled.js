'use strict';

var path = require('path');
var gulp = require('gulp');
var rename = require('gulp-rename');
var conf = require('./conf');

var browserSync = require('browser-sync');
var webpack = require('webpack-stream');

var $ = require('gulp-load-plugins')();

function webpackWrapper(watch, test, callback) {
  var webpackOptions = {
    watch: watch,
    module: {
      preLoaders: [{ test: /\.js$/, exclude: /node_modules/, loader: 'eslint-loader' }],
      loaders: [{ test: /\.js$/, exclude: /node_modules/, loaders: ['ng-annotate', 'babel-loader?presets[]=es2015'] }]
    },
    output: { filename: 'index.module.js' }
  };

  if (watch) {
    webpackOptions.devtool = 'inline-source-map';
  }

  var webpackChangeHandler = function (err, stats) {
    if (err) {
      conf.errorHandler('Webpack')(err);
    }
    $.util.log(stats.toString({
      colors: $.util.colors.supportsColor,
      chunks: false,
      hash: false,
      version: false
    }));
    browserSync.reload();
    if (watch) {
      watch = false;
      callback();
    }
  };

  var sources = [path.join(conf.paths.src, '/app/index.module.js')];
  if (test) {
    sources.push(path.join(conf.paths.src, '/app/**/*.spec.js'));
  }

  return gulp.src(sources).pipe(webpack(webpackOptions, null, webpackChangeHandler)).pipe(gulp.dest(path.join(conf.paths.tmp, '/serve/app')));
}

gulp.task('i18n', function () {
  var i18n_file = conf.wiredep.directory + '/angular-i18n/angular-locale_ru-ru.js';
  return gulp.src(i18n_file).pipe(rename('angular-locale.js')).pipe(gulp.dest(conf.paths.src + '/i18n'));
});

gulp.task('scripts', ['i18n'], function () {
  return webpackWrapper(false, false);
});

gulp.task('scripts:watch', ['scripts'], function (callback) {
  return webpackWrapper(true, false, callback);
});

gulp.task('scripts:test', function () {
  return webpackWrapper(false, true);
});

gulp.task('scripts:test-watch', ['scripts'], function (callback) {
  return webpackWrapper(true, true, callback);
});

//# sourceMappingURL=scripts-compiled.js.map